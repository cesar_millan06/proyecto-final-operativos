#include <stdio.h>
#define TAM 100
#define ARCHIVO "NumerosRand1000.txt"
#define PAGINAS 1000

int lleno=0;//Checa si todos los frames estan llenos 
int e[PAGINAS + 1];//Para guardar las entradas
int ref[TAM];//Para los bits de referencia de cada frame 
int frame[TAM];
int posrpz=0;//Primer frame
int cont=0;

int reemplazoPag(int ele){
    int temp;
 /*Cuando una pagina tiene que ser reemplazada el posrpz se mueve buscando si el bit de referencia es 0 y lo saca del ciclo, si es 1 
 le da una segunda oportunidad y reestablece la referencia a 0 */
    while(ref[posrpz]!=0){
        ref[posrpz++]=0;
        if(posrpz==TAM)
            posrpz=0;
    }                        
    temp=frame[posrpz];
    frame[posrpz]=ele;
    ref[posrpz]=1;//La ultima referencia, se pone en 1
    return temp;   
}

int falloPag(int ele){
    if(lleno!=TAM){
        ref[lleno]=1;//Todos los bits se ponen en 1 despues de que se llena el frame
        frame[lleno++]=ele;
    }
    else
        reemplazoPag(ele);
}

int Buscar(int ele){
    int i,bit;
    bit=0;
    if(lleno!=0){
        for(i=0;i<lleno;i++)
            if(ele==frame[i]){
                bit=1;ref[i]=1;//Cuando una referencia de pagina aparece, el bit de referencia se pone en 1
                break;
            }
    }
    return bit;   
}

int main(){
    int n,i;
    FILE *fp;
    fp=fopen(ARCHIVO,"r");
    printf("Total de elementos de entrada: ");
    fscanf(fp,"%d",&n);
    printf("%d",n);
    for(i=0;i<n;i++)
        fscanf(fp,"%d",&e[i]);
    printf("\n----------------ELEMENTOS----------------\n");
    for(i=0;i<n;i++)
        printf("%d  ",e[i]);
    printf("\n\n");
    for(i=0;i<n;i++){
        if(Buscar(e[i])!=1){
            falloPag(e[i]);
            cont++;
        }
    }
    printf("\nTotal de fallos de pagina: %d\n",cont);
    return 0;
}