#include <stdio.h>
#define TAM 100
#define ARCHIVO "NumerosRand1000.txt"
#define PAGINAS 1000

int lleno=0;//Checa si todos los frames estan llenos 
int e[PAGINAS + 1],n;//Para guardar las entradas
int frame[TAM];
int contFreq[TAM]={0};//Contador de frecuencia de uso 

int posrpz;
int cont=0;


int mayorOpc(){
    int i,min;
    min=0;
    for(i=0;i<TAM;i++){//La pagina con menor frecuencia es la victima 
        if(contFreq[min]>contFreq[i])
            min=i;
    }
    posrpz=min;
    return posrpz;
}

int reemplazoPag(int ele){
    int temp;
    posrpz=mayorOpc();//Funcion de seleccion de victima
    temp=frame[posrpz];
    frame[posrpz]=ele;
    contFreq[posrpz]=1;//Nueva pagina = contador a 1 
    return temp;   
}

int falloPag(int ele){
    if(lleno!=TAM){
        contFreq[lleno]++;//Se incrementan contadores hasta que se llenan los frames
        frame[lleno++]=ele;
    }
    else
        reemplazoPag(ele);
}

int Buscar(int ele){
    int i,bit;
    bit=0;
    if(lleno!=0){
        for(i=0;i<lleno;i++)
            if(ele==frame[i]){   
                bit=1;contFreq[i]++;//Se incrementa el contado cuando se hace una referencia 
                break;
            }
        }
    return bit;   
}

int main(){
    int i;
    FILE *fp;
    fp=fopen(ARCHIVO,"r");
    printf("Total de elementos de entrada:");
    fscanf(fp,"%d",&n);
    printf("%d",n);
    for(i=0;i<n;i++)
        fscanf(fp,"%d",&e[i]);
    printf("\n----------------ELEMENTOS----------------\n");
    for(i=0;i<n;i++)
        printf("%d  ",e[i]);
    printf("\n\n");
    for(i=0;i<n;i++)
    {
        if(Buscar(e[i])!=1){
            falloPag(e[i]);
            cont++;
        }      
    }
    printf("\nTotal de fallos de pagina: %d\n",cont);
    return 0;
}
    
    
