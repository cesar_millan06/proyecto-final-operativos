#include <stdio.h>
#define TAM 100
#define ARCHIVO "NumerosRand1000.txt"
#define PAGINAS 1000

int lleno=0;//Checa si todos los frames estan llenos 
int e[PAGINAS + 1];//Para guardar las entradas

int frame[TAM];
int posrpz=0;//Posicion de la pagina que necesita ser reemplazada
int cont=0;//Contador de los fallos de pagina
int contRemplazo=0;

int reemplazoPag(int ele){
    int temp;
    temp=frame[posrpz];
    frame[posrpz]=ele;
    posrpz++;//Se mueve al siguiente frame 
    if(posrpz==TAM)
        posrpz=0;
    contRemplazo++;
    return temp;   //Devuelve a la victima
}

int falloPag(int ele){
    if(lleno!=TAM)
        frame[lleno++]=ele;//Hasta que todos los frames se llenen no se hace el reemplazamiento de pagina 
    else
        reemplazoPag(ele);//Reemplazo de pagina
}

int Buscar(int ele){//Regresa un bit que dice si la pagina esta presente o no en el frame
    int i,bit;
    bit=0;
    if(lleno!=0)
    {
        for(i=0;i<lleno;i++)
            if(ele==frame[i]){
                bit=1;
                break;
            }
    }
    return bit;   
}

int main(){
    int n,i;
    FILE *fp;//Para las entradas desde archivo
   
    fp=fopen(ARCHIVO,"r");
    printf("Total de elementos de entrada :");
    fscanf(fp,"%d",&n);
    printf("%d",n);
    for(i=0;i<n;i++)
        fscanf(fp,"%d",&e[i]);
    fclose(fp);
    printf("\n----------------ELEMENTOS----------------\n");
    for(i=0;i<n;i++)
        printf("%d  ",e[i]);
    printf("\n\n");
   
    for(i=0;i<n;i++){
        if(Buscar(e[i])!=1){
            falloPag(e[i]);
            cont++;
        }
    }
    printf("\nTotal de fallos de pagina: %d\n",cont);
    printf("\nTotal de remplazo de página: %d\n",contRemplazo);

    return 0;
}
    
    
