#include <stdio.h>
#define TAM 100
#define ARCHIVO "NumerosRand1000.txt"
#define PAGINAS 1000

int lleno=0;//Checa si todos los frames estan llenos 
int e[PAGINAS + 1],n;//Para guardar las entradas
int frame[TAM];
int contFreq[TAM]={0};
int f=0;//Contador por cada vez de uso
int posrpz;
int cont=0;

int menorOpc(){//Busca la pagina menos reciente usando los valores del contador
    int i,min;
    min=0;
    for(i=0;i<TAM;i++)
        if(contFreq[min]>contFreq[i])
            min=i;
    return min;
}

int reemplazoPag(int ele){
    int temp;
    posrpz=menorOpc();
    temp=frame[posrpz];
    frame[posrpz]=ele;
    contFreq[posrpz]=f;//Se guarda el contador cada vez que la pagina se usa 
    return temp;   
}

int falloPag(int ele){
    if(lleno!=TAM){
        contFreq[lleno]=f;//Darle valor al contador de cada vez de uso 
        frame[lleno++]=ele;
    }
    else
        reemplazoPag(ele);
}

int Buscar(int ele){
    int i,bit;
    bit=0;
    if(lleno!=0){
        for(i=0;i<lleno;i++)
            if(ele==frame[i]){
                bit=1;
                contFreq[i]=f;//Si no hay fallo pero el elemento esta referenciado el contador se vuelve el contador de cada vez de uso 
                break;
            }
    }
    return bit;   
}

int main(){
    int i;
    FILE *fp;
    fp=fopen(ARCHIVO,"r");
    printf("Total de elementos de entrada: ");
    fscanf(fp,"%d",&n);
    printf("%d",n);
    for(i=0;i<n;i++)
        fscanf(fp,"%d",&e[i]);
    printf("\n----------------ELEMENTOS----------------\n");
    for(i=0;i<n;i++)
        printf("%d  ",e[i]);
    printf("\n\n");
    for(i=0;i<n;i++){
        f++;//Se incrementa el uso
        if(Buscar(e[i])!=1){
            falloPag(e[i]);
            cont++;
        }  
    }
    printf("\nTotal de fallos de pagina: %d\n",cont);
    return 0;
}
    
    
