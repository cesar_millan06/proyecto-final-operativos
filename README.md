# Remplazo de página #
## Proyecto final de la materia Sistemas Operativos ##
### Prof. Javier Abdul Córdoba ###

Simulación de los principales algoritmos de remplazo de página, 
fundamentales para cualquier sistema operativo moderno.

El archivo de donde toma los numeros se llama "NumerosRand1000.txt" donde el primer 
número indica la cantida de números que vienen a continuación. Todos los algoritmos 
usan el mismo archivo, sólo que se deben de compilar y ejecutar por separado. 
Al principio de cada archivo viene en defines el tamaño de los frames, el nombre 
del archivo txt y el número de páginas que va a recibir. Otro dato importante es 
que los números aleatorios son 1000 números entre el 0 y 1500.

Resultados:
Fifo: 930 fallos de página.
LFU: 945 fallos de página.
LRU: 930 fallos de página.
MFU: 946 fallos de página.
OPT: 930 fallos de página.
Second Chance: 934 fallos de página.

**El problema es que esta quedando un conteo de fallo de página casi iguales en todos los algoritmos cuando no debería de ser así.**

Integrantes:
- Alejandra Garay
- Victor Hugo Benitez
- César Millán
