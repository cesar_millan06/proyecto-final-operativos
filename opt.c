#include <stdio.h>
#define TAM 100
#define ARCHIVO "NumerosRand1000.txt"
#define PAGINAS 1000

int lleno=0;//Checa si todos los frames estan llenos
int e[PAGINAS + 1],n;//Para guardar las entradas
int frame[TAM];
int f=0; 
int posrpz;
int cont=0;


int mayorOpc(){
    int temp[TAM]={0};//Checa la aparicion de las posibles paginas mas cercanas a futuro
    int c=0;//Contador para salir del ciclo cuando se obtiene las 2 paginas mas cercanas a futuro 
    int id,i,k,j=TAM;
    id=0;
    for(i=f+1;i<n;i++){//Checa del uso actual hasta el final de las futuras referencias
        for(k=0;k<j;k++){//Checar si la pagina esta en el futuro o no 
            if(e[i]==frame[k]){
                if(temp[k]!=1){
                    temp[k]=1;
                    c++;
                }
            break;
            }
        }
    if(c==2)
        break;//A las dos paginas
    }
    
    id=0;
    while(id!=TAM){
        if(temp[id]==0)//Indice de la victima 
            break;
        id++;
    }
    return id;//Valor de la victima
}

int reemplazoPag(int ele){
    int temp;
    posrpz=mayorOpc();
    temp=frame[posrpz];
    frame[posrpz]=ele;
    return temp;   
}

int falloPag(int ele){
    if(lleno!=TAM)
        frame[lleno++]=ele;
    else
        reemplazoPag(ele);
}

int Buscar(int ele){
    int i,bit;
    bit=0;
    if(lleno!=0){
        for(i=0;i<lleno;i++)
            if(ele==frame[i]){
                bit=1;
                break;
            }
    }
    return bit;   
}

int main(){
    int i;
    FILE *fp;
    fp=fopen(ARCHIVO,"r");
    printf("Total de elementos de entrada:");
    fscanf(fp,"%d",&n);
    printf("%d",n);
    for(i=0;i<n;i++)
        fscanf(fp,"%d",&e[i]);
    printf("\n----------------ELEMENTOS----------------\n");
    for(i=0;i<n;i++)
        printf("%d  ",e[i]);
    printf("\n\n");
    for(i=0;i<n;i++){
        f=i;
        if(Buscar(e[i])!=1){
            falloPag(e[i]);
            cont++;
        }
    }
    printf("\nTotal de fallos de pagina: %d\n",cont);
    return 0;
}
    
    
